const router = require('express').Router();
const auth = require('../controllers/authController');
const restrict = require('../middlewares/restrict');

router.post('/register', auth.registerApi);
router.post('/login', auth.loginApi);

router.get('/create-room', restrict, auth.roomPage);
router.post('/create-room', restrict, auth.createRoom);

module.exports = router;