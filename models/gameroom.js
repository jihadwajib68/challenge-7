'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static createRoom = ({ roomName, roomCreatedBy }) => {
      return this.create({ roomName, roomCreatedBy });
    }
  }
  GameRoom.init({
    roomName: DataTypes.STRING,
    roomCreatedBy: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'GameRoom',
  });
  return GameRoom;
};