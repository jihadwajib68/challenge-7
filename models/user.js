'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static #endcrypt = (password) => bcrypt.hashSync(password, 10);

    static register_fe = ({ username, password, email }) => {
      const encryptedPassword = this.#endcrypt(password);

      return this.create({ username, password: encryptedPassword, email, isAdmin: 0 });
    }

    static register_api = async ({ username, password, email }) => {
      const encryptedPassword = this.#endcrypt(password);

      try {
        const create = await this.create({ username, password: encryptedPassword, email, isAdmin: 1 });
        if(!create) return Promise.reject('Periksa kembali data register anda.');

        return Promise.resolve(create);
      } catch (error) {
        return Promise.reject(error);
      }
    }

    checkPassword = password => bcrypt.compareSync(password, this.password);

    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username }});
        if(!user) return Promise.reject("Periksa kembali data login anda.");

        const isPasswordValid = user.checkPassword(password);
        if(!isPasswordValid) return Promise.reject("Periksa kembali data login anda.");
        
        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    }

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username
      }

      const secret = 'mastergame';
      const token = jwt.sign(payload, secret);

      return token;
    }
  }
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};